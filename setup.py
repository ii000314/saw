import setuptools

setuptools.setup(
    name='saw',
    version='0.3.5',
    author='Valentine Nikonovich',
    author_email='ii000314@gmail.com',
    description='Solution Architect Workbench',
    python_requires='>3.8',
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
        'jsonschema',
        'pyyaml',
        'jinja2',
        'gitpython',
        'plantuml',
        'markdown',
        'requests',
        'atlassian-python-api',
        'python-slugify',
        'python-pptx',
        'Pillow',
    ],
    entry_points='''
        [console_scripts]
        saw=saw:cli
    ''',
)
