import os
import pathlib
import unittest

from click.testing import CliRunner
from saw import cli

class TestSolutionArchitectureFramework(unittest.TestCase):
    def test_create_solution_success(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            result = runner.invoke(cli, ['new', name])
            self.assertEqual(result.exit_code, 0)

            os.chdir(pathlib.Path.cwd() / name)

            expected = [
                '.git', '.gitignore', '.workbench', 'Constraints', 'Settings.yaml',
                'General.yaml', 'Rationale.md', 'README.md', 'Decisions',
                'Requirements'
            ]

            actual = list(map(lambda x: str(x), pathlib.Path('.').iterdir()))

            expected.sort()
            actual.sort()
            self.assertEqual(expected, actual)

    def test_create_solution_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            result1 = runner.invoke(cli, ['new', name])
            self.assertEqual(result1.exit_code, 0)

            result2 = runner.invoke(cli, ['new', name])
            self.assertEqual(result2.exit_code, 1)

    def test_create_requirement_success(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            identifier1 = 'Test'
            result1 = runner.invoke(cli, ['req', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Requirements/{identifier1}/{identifier1}.yaml',
                f'Requirements/{identifier1}/{identifier1}.md',
            ]

            req1_dir = pathlib.Path('Requirements') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Test.Test'
            identifier2 = 'Test'
            result2 = runner.invoke(cli, ['req', tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Requirements/{identifier1}/{identifier2}/{identifier2}.yaml',
                f'Requirements/{identifier1}/{identifier2}/{identifier2}.md',
            ]

            req2_dir = pathlib.Path('Requirements') / identifier1 / identifier2
            actual2 = list(map(lambda x: str(x), req2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)

    def test_create_requirement_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag = 'Test.Test'
            result = runner.invoke(cli, ['req', tag])
            self.assertEqual(result.exit_code, 1)

    def test_create_decision_success(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            identifier1 = 'Test'
            result1 = runner.invoke(cli, ['dec', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Decisions/{identifier1}/{identifier1}.yaml',
                f'Decisions/{identifier1}/{identifier1}.md',
            ]

            req1_dir = pathlib.Path('Decisions') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Test.Test'
            identifier2 = 'Test'
            result2 = runner.invoke(cli, ['dec', tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Decisions/{identifier1}/{identifier2}/{identifier2}.yaml',
                f'Decisions/{identifier1}/{identifier2}/{identifier2}.md',
            ]

            req2_dir = pathlib.Path('Decisions') / identifier1 / identifier2
            actual2 = list(map(lambda x: str(x), req2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)

    def test_create_decision_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag = 'Test.Test'
            result = runner.invoke(cli, ['dec', tag])
            self.assertEqual(result.exit_code, 1)


    def test_create_constraint_success(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            identifier1 = 'Test'
            result1 = runner.invoke(cli, ['con', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Constraints/{identifier1}/{identifier1}.yaml',
                f'Constraints/{identifier1}/{identifier1}.md',
            ]

            req1_dir = pathlib.Path('Constraints') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Test.Test'
            identifier2 = 'Test'
            result2 = runner.invoke(cli, ['con', tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Constraints/{identifier1}/{identifier2}/{identifier2}.yaml',
                f'Constraints/{identifier1}/{identifier2}/{identifier2}.md',
            ]

            req2_dir = pathlib.Path('Constraints') / identifier1 / identifier2
            actual2 = list(map(lambda x: str(x), req2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)


    def test_create_requirement_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag = 'Test.Test'
            result = runner.invoke(cli, ['con', tag])
            self.assertEqual(result.exit_code, 1)


    def test_build_empty(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag = 'Test.Test'
            result = runner.invoke(cli, ['dec', tag])
            self.assertEqual(result.exit_code, 1)


    def test_build_with_requirements(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            runner.invoke(cli, ['req', 'Test'])

            result = runner.invoke(cli, ['build'])
            self.assertEqual(result.exit_code, 0)


    def test_build_with_decision(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            runner.invoke(cli, ['dec', 'Test'])

            result = runner.invoke(cli, ['build'])
            self.assertEqual(result.exit_code, 0)


    def test_build_with_constraint(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            runner.invoke(cli, ['con', 'Test'])

            result = runner.invoke(cli, ['build'])
            self.assertEqual(result.exit_code, 0)


    def test_build_with_req_dec_con(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            runner.invoke(cli, ['req', 'Test'])
            runner.invoke(cli, ['dec', 'Test'])
            runner.invoke(cli, ['con', 'Test'])

            result = runner.invoke(cli, ['build'])
            self.assertEqual(result.exit_code, 0)


    def test_move_req_success(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            result1 = runner.invoke(cli, ['req', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Requirements/{tag1}/{tag1}.yaml',
                f'Requirements/{tag1}/{tag1}.md',
            ]

            req1_dir = pathlib.Path('Requirements') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Hello'
            result2 = runner.invoke(cli, ['move', 'req', tag1, tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Requirements/{tag2}/{tag2}.yaml',
                f'Requirements/{tag2}/{tag2}.md',
            ]

            req2_dir = pathlib.Path('Requirements') / tag2
            actual2 = list(map(lambda x: str(x), req2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)

    def test_move_req_old_not_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            tag1 = 'Tag1'
            tag2 = 'Tag2'
            result = runner.invoke(cli, ['move', 'req', tag1, tag2])
            self.assertEqual(result.exit_code, 1)

    def test_move_req_new_already_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test1'
            result1 = runner.invoke(cli, ['req', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Requirements/{tag1}/{tag1}.yaml',
                f'Requirements/{tag1}/{tag1}.md',
            ]

            req1_dir = pathlib.Path('Requirements') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Test2'
            result2 = runner.invoke(cli, ['req', tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Requirements/{tag2}/{tag2}.yaml',
                f'Requirements/{tag2}/{tag2}.md',
            ]

            req2_dir = pathlib.Path('Requirements') / tag2
            actual2 = list(map(lambda x: str(x), req2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)

            result3 = runner.invoke(cli, ['move', 'req', tag1, tag2])
            self.assertEqual(result3.exit_code, 1)

    def test_move_req_new_parent_not_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            result1 = runner.invoke(cli, ['req', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Requirements/{tag1}/{tag1}.yaml',
                f'Requirements/{tag1}/{tag1}.md',
            ]

            req1_dir = pathlib.Path('Requirements') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Hello.World'
            result2 = runner.invoke(cli, ['move', 'req', tag1, tag2])
            self.assertEqual(result2.exit_code, 1)

    def test_move_dec_success(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            result1 = runner.invoke(cli, ['dec', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Decisions/{tag1}/{tag1}.yaml',
                f'Decisions/{tag1}/{tag1}.md',
            ]

            req1_dir = pathlib.Path('Decisions') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Hello'
            result2 = runner.invoke(cli, ['move', 'dec', tag1, tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Decisions/{tag2}/{tag2}.yaml',
                f'Decisions/{tag2}/{tag2}.md',
            ]

            req2_dir = pathlib.Path('Decisions') / tag2
            actual2 = list(map(lambda x: str(x), req2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)

    def test_move_dec_old_not_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            tag1 = 'Tag1'
            tag2 = 'Tag2'
            result = runner.invoke(cli, ['move', 'dec', tag1, tag2])
            self.assertEqual(result.exit_code, 1)

    def test_move_dec_new_already_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test1'
            result1 = runner.invoke(cli, ['dec', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Decisions/{tag1}/{tag1}.yaml',
                f'Decisions/{tag1}/{tag1}.md',
            ]

            req1_dir = pathlib.Path('Decisions') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Test2'
            result2 = runner.invoke(cli, ['dec', tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Decisions/{tag2}/{tag2}.yaml',
                f'Decisions/{tag2}/{tag2}.md',
            ]

            req2_dir = pathlib.Path('Decisions') / tag2
            actual2 = list(map(lambda x: str(x), req2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)

            result3 = runner.invoke(cli, ['move', 'dec', tag1, tag2])
            self.assertEqual(result3.exit_code, 1)

    def test_move_dec_new_parent_not_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            result1 = runner.invoke(cli, ['dec', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Decisions/{tag1}/{tag1}.yaml',
                f'Decisions/{tag1}/{tag1}.md',
            ]

            req1_dir = pathlib.Path('Decisions') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Hello.World'
            result2 = runner.invoke(cli, ['move', 'dec', tag1, tag2])
            self.assertEqual(result2.exit_code, 1)


    def test_move_con_success(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            result1 = runner.invoke(cli, ['con', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Constraints/{tag1}/{tag1}.yaml',
                f'Constraints/{tag1}/{tag1}.md',
            ]

            con1_dir = pathlib.Path('Constraints') / tag1
            actual1 = list(map(lambda x: str(x), con1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Hello'
            result2 = runner.invoke(cli, ['move', 'con', tag1, tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Constraints/{tag2}/{tag2}.yaml',
                f'Constraints/{tag2}/{tag2}.md',
            ]

            con2_dir = pathlib.Path('Constraints') / tag2
            actual2 = list(map(lambda x: str(x), con2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)


    def test_move_con_old_not_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            tag1 = 'Tag1'
            tag2 = 'Tag2'
            result = runner.invoke(cli, ['move', 'con', tag1, tag2])
            self.assertEqual(result.exit_code, 1)


    def test_move_con_new_already_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test1'
            result1 = runner.invoke(cli, ['con', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Constraints/{tag1}/{tag1}.yaml',
                f'Constraints/{tag1}/{tag1}.md',
            ]

            req1_dir = pathlib.Path('Constraints') / tag1
            actual1 = list(map(lambda x: str(x), req1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Test2'
            result2 = runner.invoke(cli, ['con', tag2])
            self.assertEqual(result2.exit_code, 0)

            expected2 = [
                f'Constraints/{tag2}/{tag2}.yaml',
                f'Constraints/{tag2}/{tag2}.md',
            ]

            con2_dir = pathlib.Path('Constraints') / tag2
            actual2 = list(map(lambda x: str(x), con2_dir.iterdir()))

            expected2.sort()
            actual2.sort()
            self.assertEqual(expected2, actual2)

            result3 = runner.invoke(cli, ['move', 'con', tag1, tag2])
            self.assertEqual(result3.exit_code, 1)


    def test_move_con_new_parent_not_exists_error(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            name = 'solution'
            runner.invoke(cli, ['new', name])
            os.chdir(pathlib.Path('.') / name)

            tag1 = 'Test'
            result1 = runner.invoke(cli, ['con', tag1])
            self.assertEqual(result1.exit_code, 0)

            expected1 = [
                f'Constraints/{tag1}/{tag1}.yaml',
                f'Constraints/{tag1}/{tag1}.md',
            ]

            con1_dir = pathlib.Path('Constraints') / tag1
            actual1 = list(map(lambda x: str(x), con1_dir.iterdir()))

            expected1.sort()
            actual1.sort()
            self.assertEqual(expected1, actual1)

            tag2 = 'Hello.World'
            result2 = runner.invoke(cli, ['move', 'con', tag1, tag2])
            self.assertEqual(result2.exit_code, 1)


if __name__ == '__main__':
    unittest.main()