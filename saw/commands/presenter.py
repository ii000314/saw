import io
import shutil
import pathlib

from pptx import Presentation
from pptx.util import Inches, Pt
from slugify import slugify
from PIL import Image

from saw.utils.builders import build_context, build_diagrams
from saw.utils.loggers import display_success
from saw.utils.settings import load_settings
from saw.utils.validators import stop_if_not_workbench


PRESENTATION_TITLE_SIZE = 30
PRESENTATION_LIST_SIZE = 20
PRESENTATION_TEXT_SIZE = 15


def run():
    """Create presentation from the solution"""

    workbench = pathlib.Path.cwd()

    stop_if_not_workbench(workbench)

    base_path = workbench / '_presentation'
    shutil.rmtree(base_path, ignore_errors=True)
    pathlib.Path(base_path).mkdir(parents=True, exist_ok=True)

    settings = load_settings(workbench)
    puml_server = settings['build']['puml']['url']

    context = build_context(workbench)
    diagrams = build_diagrams(puml_server, context)

    decision_diagrams = diagrams['decision']

    presentation = Presentation()

    add_title(presentation, context)
    add_stakeholders(presentation, context)
    add_requirements(presentation, context)
    add_decisions(presentation, context, decision_diagrams)
    add_references(presentation, context)
    add_responsible(presentation, context)

    title = context['solution']['title']

    presentation_path = base_path / f'{slugify(title)}.pptx'
    presentation.save(presentation_path)

    display_success(f'file://{presentation_path}')


def add_title(presentation, context):
    """Add title slide to presentation"""

    solution_title = context['solution']['title']

    title_slide_layout = presentation.slide_layouts[0]
    slide = presentation.slides.add_slide(title_slide_layout)
    title = slide.shapes.title

    title.text = solution_title


def add_stakeholders(presentation, context):
    """Add stakeholders slide to presentation"""

    stakeholders = context['solution']['stakeholders']

    bullet_slide_layout = presentation.slide_layouts[1]

    slide = presentation.slides.add_slide(bullet_slide_layout)
    shapes = slide.shapes

    title_shape = shapes.title
    title_shape.text = 'Stakeholders'
    title_shape.text_frame.paragraphs[0].font.size = Pt(PRESENTATION_TITLE_SIZE)

    body_shape = shapes.placeholders[1]
    tf = body_shape.text_frame

    for stakeholder in stakeholders:
        p = tf.add_paragraph()
        p.font.size = Pt(PRESENTATION_LIST_SIZE)
        p.text = stakeholder


def add_requirements(presentation, context):
    """Add requirements slides to presentation"""

    CHUNK_SIZE = 9

    reqs = context['requirements']

    chunks = [reqs[i:i + CHUNK_SIZE] for i in range(0, len(reqs), CHUNK_SIZE)]

    for chunk in chunks:
        bullet_slide_layout = presentation.slide_layouts[1]
        slide = presentation.slides.add_slide(bullet_slide_layout)
        shapes = slide.shapes

        title_shape = shapes.title
        title_shape.text = 'Requirements'
        title_shape.text_frame.paragraphs[0].font.size = Pt(PRESENTATION_TITLE_SIZE)

        body_shape = shapes.placeholders[1]
        tf = body_shape.text_frame

        for req in chunk:
            p = tf.add_paragraph()
            p.font.size = Pt(PRESENTATION_LIST_SIZE)
            p.text = req['title']


def add_decisions(presentation, context, diagrams):
    """Add architecture decision slides to presentation"""

    decision = context['decision']

    for dec in decision:
        blank_slide_layout = presentation.slide_layouts[5]
        slide = presentation.slides.add_slide(blank_slide_layout)

        shapes = slide.shapes
        title_shape = shapes.title

        title_shape.text_frame.text = dec['title']
        title_shape.text_frame.paragraphs[0].font.size = Pt(PRESENTATION_TITLE_SIZE)

        diagram = next((d['image'] for d in diagrams if d['name'] == dec['tag']), None)
        if diagram:
            image = io.BytesIO(diagram)
            (left, top, height) = calculate_coordinates(image)
            slide.shapes.add_picture(image, left, top, height=height)
        else:
            txBox = shapes.add_textbox(Inches(0.5), Inches(1.75), Inches(9), Inches(5))
            tf = txBox.text_frame
            tf.paragraphs[0].font.size = Pt(PRESENTATION_TEXT_SIZE)
            tf.text = 'TODO: Add information about architecture decision'


def calculate_coordinates(image):
    """Calculate relative image coordinates on the slide"""

    SLIDE_WIDTH_IN = 9.6
    SLIDE_HEIGHT_IN = 6.4
    SLIDE_HEIGHT_OFFSET_IN = 2

    SLIDE_HEIGHT_CORRECTED_IN = SLIDE_HEIGHT_IN - SLIDE_HEIGHT_OFFSET_IN

    px_to_in = lambda px: px * 0.0104166667

    (width_px, height_px) = Image.open(image).size

    width_in = px_to_in(width_px)
    height_in = px_to_in(height_px)

    # Resize if image higher than slide height
    if height_in > SLIDE_HEIGHT_CORRECTED_IN:
        proportion = SLIDE_HEIGHT_CORRECTED_IN / height_in

        width_in = width_in * proportion
        height_in = height_in * proportion

        height = Inches(height_in)
    else:
        height = None

    # Resize if image widther that slide width
    if width_in > SLIDE_WIDTH_IN:
        proportion = SLIDE_WIDTH_IN / width_in

        width_in = width_in * proportion
        height_in = height_in * proportion

    left_in = (SLIDE_WIDTH_IN - width_in) / 2
    left = Inches(left_in)

    top_in = (SLIDE_HEIGHT_CORRECTED_IN - height_in) / 2
    top = Inches(top_in+SLIDE_HEIGHT_OFFSET_IN)

    return (left, top, height)


def add_references(presentation, context):
    """Add references slide to presentation"""

    references = context['solution'].get('references')

    if references:
        bullet_slide_layout = presentation.slide_layouts[1]

        slide = presentation.slides.add_slide(bullet_slide_layout)
        shapes = slide.shapes

        title_shape = shapes.title
        title_shape.text = 'References'
        title_shape.text_frame.paragraphs[0].font.size = Pt(PRESENTATION_TITLE_SIZE)

        body_shape = shapes.placeholders[1]
        tf = body_shape.text_frame

        for (title, link) in references.items():
            p = tf.add_paragraph()
            p.font.size = Pt(PRESENTATION_LIST_SIZE)
            p.text = f'{title}: {link}'


def add_responsible(presentation, context):
    """Add responsible person slide to presentation"""

    responsible = context['solution'].get('responsible')

    if responsible:
        bullet_slide_layout = presentation.slide_layouts[1]

        slide = presentation.slides.add_slide(bullet_slide_layout)
        shapes = slide.shapes

        title_shape = shapes.title
        title_shape.text = 'Responsible'
        title_shape.text_frame.paragraphs[0].font.size = Pt(PRESENTATION_TITLE_SIZE)

        body_shape = shapes.placeholders[1]
        tf = body_shape.text_frame

        for resp in responsible:
            p = tf.add_paragraph()
            p.font.size = Pt(PRESENTATION_LIST_SIZE)
            p.text = resp