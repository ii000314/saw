import pathlib

from git import Repo

from saw.utils.validators import stop_if_workbench
from saw.utils.loggers import display_success
from saw.utils.loaders import load_template
from saw.utils.renderers import render_template
from saw.utils.formatters import titlefy_name

from saw.utils.constants import (
    REQUIREMENTS_DIR,
    DECISIONS_DIR,
    CONSTRAINTS_DIR,
)


def run(name):
    """Create new solution"""

    workbench = pathlib.Path.cwd() / name

    stop_if_workbench(workbench)

    Repo.init(workbench)

    context = {
        'namespace': 'General',
        'solution': titlefy_name(name),
    }

    create_layout(workbench, context)

    display_success(f'Solution "{name}" was successfully created')


def create_layout(workbench, context):
    create_workbench(workbench, context)
    create_gitignore(workbench, context)
    create_settings(workbench, context)
    create_solution(workbench, context)
    create_rationale(workbench, context)
    create_readme(workbench, context)
    create_decision(workbench, context)
    create_requirements(workbench, context)
    create_constraints(workbench, context)


def create_workbench(workbench, _context):
    """Creates 'workbench' file in the project root"""

    path = workbench / '.workbench'
    with path.open('w') as f:
        f.write(load_template('workbench.yaml'))


def create_gitignore(workbench, _context):
    """Creates '.gitignore' file in the project root"""

    path = workbench / '.gitignore'
    with path.open('w') as f:
        f.write(load_template('gitignore'))


def create_settings(workbench, _context):
    """Creates 'settings' file in the project root"""

    path = workbench / 'Settings.yaml'
    with path.open('w') as f:
        f.write(load_template('settings.yaml'))


def create_solution(workbench, context):
    """Creates 'solution' file in the project root"""

    path = workbench / 'General.yaml'
    with path.open('w') as f:
        template = load_template('solution.yaml')
        document = render_template(template, context)
        f.write(document)


def create_rationale(workbench, _context):
    """Creates 'rationale' file in the project root"""

    path = workbench / 'Rationale.md'
    with path.open('w') as f:
        f.write(load_template('solution.md'))


def create_readme(workbench, context):
    """Creates 'readme' file in the project root"""

    path = workbench / 'README.md'
    with path.open('w') as f:
        template = load_template('README.md')
        document = render_template(template, context)
        f.write(document)


def create_decision(workbench, _context):
    """Creates directory for architecture decisions in the project root"""

    path = workbench / DECISIONS_DIR
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    path = path / '.gitkeep'
    path.touch(exist_ok=False)


def create_requirements(workbench, _context):
    """Creates directory for requirements in the project root"""

    path = workbench / REQUIREMENTS_DIR
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    path = path / '.gitkeep'
    path.touch(exist_ok=False)


def create_constraints(workbench, _context):
    """Creates directory for constraints in the project root"""

    path = workbench / CONSTRAINTS_DIR
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    path = path / '.gitkeep'
    path.touch(exist_ok=False)
