import pathlib

from saw.utils.loggers import (
    display_success,
    raise_error
)

from saw.utils.validators import (
    stop_if_not_workbench,
    stop_if_parent_req_not_exists,
    stop_if_req_not_exists,
    stop_if_req_exists,
    stop_if_parent_dec_not_exists,
    stop_if_dec_not_exists,
    stop_if_dec_exists,
    stop_if_parent_con_not_exists,
    stop_if_con_not_exists,
    stop_if_con_exists,
    stop_if_invalid_tag,
)

from saw.utils.constants import (
    REQUIREMENTS_DIR,
    DECISIONS_DIR,
    CONSTRAINTS_DIR,
)


def run(obj, old, new):
    """Move object 'old' location to 'new' location"""

    workbench = pathlib.Path.cwd()

    stop_if_not_workbench(workbench)
    stop_if_invalid_tag(new)
    stop_if_invalid_tag(old)

    if obj == 'req':
        move_requirement(old, new, workbench)
    elif obj == 'dec':
        move_decision(old, new, workbench)
    elif obj == 'con':
        move_constraint(old, new, workbench)
    else:
        raise_error(f'Unexpected object type: {obj}')


def move_requirement(old, new, workbench):
    stop_if_req_not_exists(old, workbench)
    stop_if_parent_req_not_exists(new, workbench)
    stop_if_req_exists(new, workbench)

    old_path = workbench / REQUIREMENTS_DIR / old.replace('.', '/')
    new_path = workbench / REQUIREMENTS_DIR / new.replace('.', '/')

    move(old_path, new_path)

    display_success(f'Requirement "{old}" was successfully moved to "{new}"')


def move_decision(old, new, workbench):
    stop_if_dec_not_exists(old, workbench)
    stop_if_parent_dec_not_exists(new, workbench)
    stop_if_dec_exists(new, workbench)

    old_path = workbench / DECISIONS_DIR / old.replace('.', '/')
    new_path = workbench / DECISIONS_DIR / new.replace('.', '/')

    move(old_path, new_path)

    display_success(f'Decision "{old}" was successfully moved to "{new}"')


def move_constraint(old, new, workbench):
    stop_if_con_not_exists(old, workbench)
    stop_if_parent_con_not_exists(new, workbench)
    stop_if_con_exists(new, workbench)

    old_path = workbench / CONSTRAINTS_DIR / old.replace('.', '/')
    new_path = workbench / CONSTRAINTS_DIR / new.replace('.', '/')

    move(old_path, new_path)

    display_success(f'Constraint "{old}" was successfully moved to "{new}"')


def move(old_path, new_path):
    old_path.replace(new_path)

    for source in new_path.iterdir():
        if not source.is_dir():
            destination = new_path / f'{new_path.name}{source.suffix}'
            source.replace(destination)
