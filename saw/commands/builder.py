import shutil
import pathlib

from saw.utils.settings import load_settings
from saw.utils.loggers import display_success
from saw.utils.validators import stop_if_not_workbench
from saw.utils.mappers import chapter_to_prefix

from saw.utils.builders import (
    build_context,
    build_diagrams,
    build_html,
)


def run():
    """Build html document of current solution"""

    workbench = pathlib.Path.cwd()

    stop_if_not_workbench(workbench)

    base_path = workbench / '_build'
    shutil.rmtree(base_path, ignore_errors=True)
    pathlib.Path(base_path).mkdir(parents=True, exist_ok=True)

    settings = load_settings(workbench)
    puml_server = settings['build']['puml']['url']

    context = build_context(workbench)

    all_diagrams = build_diagrams(puml_server, context)
    for (chapter, diagrams) in all_diagrams.items():
        prefix = chapter_to_prefix(chapter)
        for diagram in diagrams:
            d = base_path / f'{prefix}.{diagram["name"]}.png'
            with d.open('wb') as f:
                f.write(diagram['image'])

    document = build_html(context)

    path = base_path / 'index.html'
    with path.open('w') as f:
        f.write(document)

    display_success(f'file://{path}')
