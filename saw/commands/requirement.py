import pathlib

from saw.utils.loaders import load_template
from saw.utils.renderers import render_template
from saw.utils.loggers import display_success
from saw.utils.formatters import titlefy_tag
from saw.utils.constants import REQUIREMENTS_DIR
from saw.utils.validators import (
    validate_requirement_tag,
    stop_if_not_workbench,
)


def run(tag):
    """Create new requirement"""

    workbench = pathlib.Path.cwd()

    stop_if_not_workbench(workbench)
    validate_requirement_tag(tag, workbench)

    path = workbench / REQUIREMENTS_DIR / tag.replace('.', '/')
    pathlib.Path(path).mkdir(parents=True, exist_ok=False)

    identifier = tag.split('.')[-1]

    settings = path / f'{identifier}.yaml'
    with settings.open('w') as f:
        template = load_template('requirement.yaml')
        context = {'title': titlefy_tag(identifier)}
        document = render_template(template, context)
        f.write(document)

    description = path / f'{identifier}.md'
    with description.open('w') as f:
        f.write(load_template('requirement.md'))

    display_success(f'Requirement "{tag}" was successfully created')
