import urllib
import pathlib
import requests

from base64 import b64encode
from atlassian import Confluence
from saw.utils.mappers import chapter_to_prefix
from saw.utils.settings import (
    load_settings,
    resolve_plain_param,
    resolve_secret_param,
)
from saw.utils.loggers import (
    display_success,
    raise_error,
)
from saw.utils.builders import (
    build_context,
    build_diagrams,
    build_html,
    build_confluence,
)
from saw.utils.validators import stop_if_not_workbench

def run():
    """Upload solution to documentation server"""

    workbench = pathlib.Path.cwd()

    stop_if_not_workbench(workbench)

    settings = load_settings(workbench)
    hostings = settings['upload'] or []
    puml_server = settings['build']['puml']['url']

    context = build_context(workbench)
    diagrams = build_diagrams(puml_server, context)

    for (hosting, config) in hostings.items():
        if hosting == 'archive':
            upload_to_archive(config, context, diagrams)
        elif hosting == 'confluence':
            upload_to_confluence(config, context, diagrams)


def upload_to_archive(config, context, all_diagrams):
    """Upload solution to 'Archive' documentation server"""

    html_bytes = build_html(context).encode('utf-8')
    html_bytes_base64 = b64encode(html_bytes)

    content = [{
        'name': 'index.html',
        'data': html_bytes_base64.decode('utf-8'),
    }]

    for (chapter, diagrams) in all_diagrams.items():
        prefix = chapter_to_prefix(chapter)
        for diagram in diagrams:
            name = diagram['name']
            image = diagram['image']
            b64_data = b64encode(image)
            utf8_data = b64_data.decode('utf-8')
            obj = {
                'name': f'{prefix}.{name}.png',
                'data': utf8_data,
            }
            content.append(obj)

    namespace = context['solution']['namespace']
    title = context['solution']['title']
    try:
        url = config["url"]
        endpoint = f'/{namespace}/{title}/'
        res = requests.post(urllib.parse.urljoin(url, endpoint), json=content)
        if res.status_code == 201:
            display_success('Document was successfully uploaded to Archive')
        else:
            raise_error(f'Upload error. Details: {res.text}')
    except requests.exceptions.ConnectionError as err:
        raise_error(f'Connection error. Details: \n {err}')
    except Exception as err:
        raise_error(f'Unexpected error. Details: \n {err}')


def upload_to_confluence(config, context, all_diagrams):
    username = resolve_plain_param('Username', config['username'])
    password = resolve_secret_param('Password', config['password'])

    confluence = Confluence(
        url=config['url'],
        username=username,
        password=password,
    )

    namespace = context['solution']['namespace']
    title = context['solution']['title']
    branch = context['repo']['branch']

    space = config['space']
    page_title = f'[{namespace}] {title} ({branch})'
    body = build_confluence(context)

    if parent := config.get('parent', None):
        parent_id = confluence.get_page_id(space, parent)
    else:
        parent_id = None

    if confluence.page_exists(space, page_title):
        page_id = confluence.get_page_id(space, page_title)
        resp = confluence.update_page(page_id, page_title, body, parent_id=parent_id)
    else:
        resp = confluence.create_page(space, page_title, body, parent_id=parent_id)
        page_id = resp['id']

    for (chapter, diagrams) in all_diagrams.items():
        prefix = chapter_to_prefix(chapter)
        for diagram in diagrams:
            name = f'{prefix}.{diagram["name"]}.png'
            image = diagram['image']
            confluence.attach_content(image, name, page_id=page_id, content_type='image/png')

    url = config["url"]
    endpoint = f'/pages/viewpage.action?pageId={resp["id"]}'
    display_success(urllib.parse.urljoin(url, endpoint))
