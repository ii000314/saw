import pathlib

from saw.utils.loaders import load_template
from saw.utils.renderers import render_template
from saw.utils.loggers import display_success
from saw.utils.formatters import titlefy_tag
from saw.utils.constants import DECISIONS_DIR
from saw.utils.validators import (
    validate_decision_tag,
    stop_if_not_workbench
)



def run(tag):
    """Create new architecture decision"""

    workbench = pathlib.Path.cwd()

    stop_if_not_workbench(workbench)
    validate_decision_tag(tag, workbench)

    path = workbench / DECISIONS_DIR / tag.replace('.', '/')
    pathlib.Path(path).mkdir(parents=True, exist_ok=False)

    identifier = tag.split('.')[-1]

    settings = path / f'{identifier}.yaml'
    with settings.open('w') as f:
        template = load_template('decision.yaml')
        context = {'title': titlefy_tag(identifier)}
        document = render_template(template, context)
        f.write(document)

    description = path / f'{identifier}.md'
    with description.open('w') as f:
        f.write(load_template('decision.md'))

    display_success(f'Architecture decision "{tag}" was successfully created')
