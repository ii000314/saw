# General Info

This repository contains source code of solution: **{{solution}}**.

If you want to contribute to this solution, install the [Solution Architecture Workbench](https://bitbucket.org/ii000314/saw/src/master/) and follow the instruction below.

# Contribution

Build solution:
```
saw build
```

Upload solution:
```
saw upload
```

Make presentation template:
```
saw presentation
```