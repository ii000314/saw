import os
import sys
import traceback

import click

from importlib.metadata import version

from saw.commands import (
    solution,
    requirement,
    constraint,
    decision,
    builder,
    uploader,
    presenter,
    mover,
)

from saw.utils.loggers import display_error


@click.group()
@click.version_option(version('saw'))
def cli():
    """Solution Architect Workbench"""


@click.command()
@click.argument('name', type=click.STRING)
def new(name):
    """Create new solution"""
    execute(solution.run, [name])


@click.command()
@click.argument('tag', type=click.STRING)
def req(tag):
    """Add requirement to solution"""
    execute(requirement.run, [tag])


@click.command()
@click.argument('tag', type=click.STRING)
def dec(tag):
    """Add architecture decision to solution"""
    execute(decision.run, [tag])


@click.command()
@click.argument('tag', type=click.STRING)
def con(tag):
    """Add constraint to solution"""
    execute(constraint.run, [tag])


@click.command()
def build():
    """Build report from solution"""
    execute(builder.run, [])


@click.command()
def upload():
    """Upload reports to archive"""
    execute(uploader.run, [])


@click.command()
def presentation():
    """Create presentation from the solution"""
    execute(presenter.run, [])


@click.command()
@click.argument('obj', type=click.STRING)
@click.argument('old_tag', type=click.STRING)
@click.argument('new_tag', type=click.STRING)
def move(obj, old_tag, new_tag):
    """Move 'obj' to from 'old' place to 'new' place"""
    execute(mover.run, [obj, old_tag, new_tag])


def execute(command, args):
    """Execute command and handle unexpected errors"""
    try:
        command(*args)
    except click.ClickException as err:
        display_error(err)
        sys.exit(1)
    except Exception:
        handle_unexpected_error()
        sys.exit(1)


def handle_unexpected_error():
    """Default behavior for all the unexpected errors"""
    if os.getenv('DEBUG') == '1':
        display_error(traceback.format_exc())
    else:
        display_error('Unexpected error. Use DEBUG=1 to see the details.')


cli.add_command(new)
cli.add_command(req)
cli.add_command(dec)
cli.add_command(con)
cli.add_command(build)
cli.add_command(upload)
cli.add_command(presentation)
cli.add_command(move)
