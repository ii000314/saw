import click


def display_success(msg):
    click.echo(click.style(f'[SUCCESS]: {msg}', fg='green'))


def display_warning(msg):
    click.echo(click.style(f'[WARNING]: {msg}', fg='yellow'))


def display_error(msg):
    click.echo(click.style(f'[ERROR]: {msg}', fg='red'))


def raise_error(msg):
    raise click.ClickException(msg)