def chapter_to_prefix(chapter):
    """Maps chapter of document to artifact prefix"""

    mapping = {
        'decisions': 'Dec',
    }
    return mapping[chapter]
