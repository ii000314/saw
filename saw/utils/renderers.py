from jinja2 import Template


def render_template(template, context):
    """Renders template using specified context"""

    template = Template(template)
    document = template.render(**context)
    return document