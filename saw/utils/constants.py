""" Set of frequently used constants """

REQUIREMENTS_DIR = 'Requirements'
DECISIONS_DIR = 'Decisions'
CONSTRAINTS_DIR = 'Constraints'
MAX_PRIORITY = 99
