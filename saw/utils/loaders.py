import re
import json
from markdown import extensions
from saw.utils.loggers import raise_error
import yaml
import pathlib
import markdown

from git import Repo

from saw.utils.tree import (
    flatten
)

from saw.utils.validators import (
    warn_if_dec_does_not_cover_requirement,
    stop_if_headers_inside_description,
    stop_if_invalid_stakeholders,
    stop_if_invalid_object,
)

from saw.utils.constants import (
    REQUIREMENTS_DIR,
    DECISIONS_DIR,
    CONSTRAINTS_DIR,
    MAX_PRIORITY,
)


def load_solution(workbench):
    """Load solution object from workbench"""

    obj = None

    solution = workbench / 'General.yaml'
    with solution.open('r') as f:
        obj = yaml.full_load(f)

    rationale = workbench / 'Rationale.md'
    with rationale.open('r') as f:
        obj['description'] = markdown.markdown(f.read(), extensions=['fenced_code'])

    if 'stakeholders' in obj:
        obj['stakeholders'].sort()
    else:
        obj['stakeholders'] = []

    stop_if_invalid_object(obj, load_schema('solution.json'))
    stop_if_headers_inside_description(obj)

    return obj


def load_repo(workbench):
    """Load Git repo object from workbench"""

    repo = Repo(workbench)
    remotes = [(r.name, r.url) for r in repo.remotes]
    obj = {
        'location': workbench,
        'remotes': remotes,
        'branch': repo.active_branch.name,
    }
    return obj


def load_template(template):
    """Loads template by name"""

    path = pathlib.Path(__file__).parents[1] / 'templates' / template
    with path.open('r') as f:
        template = f.read()
    return template


def load_schema(schema):
    """Loads schema by name"""

    path = pathlib.Path(__file__).parents[1] / 'schemas' / schema
    with path.open('r') as f:
        schema = json.load(f)
    return schema


def load_requirements(workbench):
    """Loads list of requirements from workbench"""

    general = load_solution(workbench)

    stakehodlers = general['stakeholders'] + general['responsible']

    path = workbench / REQUIREMENTS_DIR
    tree = load_requirements_tree(path)
    requirements = flatten(tree)

    stop_if_invalid_stakeholders(stakehodlers, requirements)

    return requirements


def load_requirements_tree(path, parents=[]):
    requirements = []
    directories = [p for p in path.iterdir() if p.is_dir()]
    for directory in directories:
        obj = None
        identifier = directory.name

        settings = directory / f'{identifier}.yaml'
        with settings.open('r') as f:
            obj = yaml.full_load(f)

        description = directory / f'{identifier}.md'
        with description.open('r') as f:
            extensions = ['fenced_code', 'codehilite']
            parts = re.split('\\n=+', f.read())
            if len(parts) == 1:
                obj['rationale'] = markdown.markdown(parts[0], extensions=extensions)
            elif len(parts) == 2:
                obj['rationale'] = markdown.markdown(parts[0], extensions=extensions)
                obj['description'] = markdown.markdown(parts[1], extensions=extensions)
            elif len(parts) == 3:
                obj['rationale'] = markdown.markdown(parts[0], extensions=extensions)
                obj['description'] = markdown.markdown(parts[1], extensions=extensions)
                obj['verification'] = markdown.markdown(parts[2], extensions=extensions)
            else:
                raise_error('Unexpected amount of parts in requirement')

        stop_if_invalid_object(obj, load_schema('requirement.json'))
        stop_if_headers_inside_description(obj)

        labels = parents + [identifier]

        if not obj.get('stakeholders'):
            obj['stakeholders'] = []

        if not obj.get('approved'):
            obj['approved'] = []

        obj['tag'] = '.'.join(labels)
        obj['deepness'] = len(labels)
        obj['identifier'] = identifier
        obj['children'] = load_requirements_tree(directory, labels)
        obj['stakeholders'].sort()

        requirements.append(obj)

    requirements.sort(key=lambda obj: obj.get('priority', MAX_PRIORITY))

    return requirements


def load_decisions(workbench):
    """Load list of decisions from workbench"""

    requirements = load_requirements(workbench)

    path = workbench / DECISIONS_DIR
    tree = load_decisions_tree(requirements, path)
    decisions = flatten(tree)

    warn_if_dec_does_not_cover_requirement(decisions)

    return decisions


def load_decisions_tree(requirements, path, parents=[]):
    decisions = []
    directories = [p for p in path.iterdir() if p.is_dir()]
    for directory in directories:
        obj = None
        identifier = directory.name

        settings = directory / f'{identifier}.yaml'
        with settings.open('r') as f:
            obj = yaml.full_load(f)

        description = directory / f'{identifier}.md'
        if description.exists():
            with description.open('r') as f:
                extensions = ['fenced_code', 'codehilite']
                parts = re.split('\\n=+', f.read())
                if len(parts) == 1:
                    obj['description'] = markdown.markdown(parts[0], extensions=extensions)
                elif len(parts) == 2:
                    obj['description'] = markdown.markdown(parts[0], extensions=extensions)
                    obj['implementation'] = markdown.markdown(parts[1], extensions=extensions)
                elif len(parts) == 3:
                    obj['description'] = markdown.markdown(parts[0], extensions=extensions)
                    obj['implementation'] = markdown.markdown(parts[1], extensions=extensions)
                    obj['estimation'] = markdown.markdown(parts[2], extensions=extensions)
                else:
                    raise_error('Unexpected amount of parts in decision')

        diagram = directory / f'{identifier}.puml'
        if diagram.exists():
            with diagram.open('r') as f:
                obj['diagram'] = f.read()

        stop_if_invalid_object(obj, load_schema('decision.json'))
        stop_if_headers_inside_description(obj)

        labels = parents + [identifier]
        decision_tag = '.'.join(labels)

        if not obj.get('approved'):
            obj['approved'] = []

        priorities = []
        for req in requirements:
            if req.get('tag') in obj.get('requirements', []):
                priorities.append(req.get('priority', MAX_PRIORITY))

        obj['tag'] = decision_tag
        obj['deepness'] = len(labels)
        obj['identifier'] = identifier
        obj['children'] = load_decisions_tree(requirements, directory, labels)
        obj['priority'] = min(priorities or [MAX_PRIORITY,])

        decisions.append(obj)

    decisions.sort(key=lambda obj: obj.get('priority', MAX_PRIORITY))

    return decisions


def load_constraints(workbench):
    """Loads list of constraints from workbench"""

    general = load_solution(workbench)

    stakehodlers = general['stakeholders'] + general['responsible']

    path = workbench / CONSTRAINTS_DIR
    tree = load_constraints_tree(path)
    requirements = flatten(tree)

    stop_if_invalid_stakeholders(stakehodlers, requirements)

    return requirements


def load_constraints_tree(path, parents=[]):
    constraints = []
    directories = [p for p in path.iterdir() if p.is_dir()]
    for directory in directories:
        obj = None
        identifier = directory.name

        settings = directory / f'{identifier}.yaml'
        with settings.open('r') as f:
            obj = yaml.full_load(f)

        description = directory / f'{identifier}.md'
        with description.open('r') as f:
            extensions = ['fenced_code', 'codehilite']
            parts = re.split('\\n=+', f.read())
            if len(parts) == 1:
                obj['description'] = markdown.markdown(parts[0], extensions=extensions)
            else:
                raise_error('Unexpected amount of parts in constraint')

        stop_if_invalid_object(obj, load_schema('constraint.json'))
        stop_if_headers_inside_description(obj)

        labels = parents + [identifier]

        if not obj.get('stakeholders'):
            obj['stakeholders'] = []

        if not obj.get('approved'):
            obj['approved'] = []

        obj['tag'] = '.'.join(labels)
        obj['deepness'] = len(labels)
        obj['identifier'] = identifier
        obj['children'] = load_requirements_tree(directory, labels)
        obj['stakeholders'].sort()

        constraints.append(obj)

    constraints.sort(key=lambda obj: obj.get('priority', MAX_PRIORITY))

    return constraints


def load_diagram_from_cache(tag, workbench):
    """Loads diagrnam from cache"""

    path = workbench / '_build' / f'Dec.{tag}.png'
    if path.exists():
        with path.open('rb') as f:
            diagram = f.read()
    else:
        diagram = None

    return diagram