import os
import yaml
import click

from saw.utils.validators import stop_if_invalid_object
from saw.utils.loaders import load_schema


def load_settings(workbench):
    s = workbench / 'Settings.yaml'
    with s.open('r') as f:
        obj = yaml.full_load(f)

    stop_if_invalid_object(obj, load_schema('settings.json'))

    return obj


def resolve_plain_param(message, value):
    return resolve_param(message, value, False)


def resolve_secret_param(message, value):
    return resolve_param(message, value, True)


def resolve_param(message, value, hide_input=False):
    if value == '<>':
        param = click.prompt(message, type=str, hide_input=hide_input)
    elif value.startswith('<') and value.endswith('>'):
        param = os.getenv(value.strip('<>'))
        if not param:
            param = click.prompt(message, type=str, hide_input=hide_input)
    else:
        param = value

    return param
