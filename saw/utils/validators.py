import os
import re
import pathlib
import jsonschema

from saw.utils.loggers import (
    display_warning,
    raise_error,
)

from saw.utils.constants import (
    REQUIREMENTS_DIR,
    DECISIONS_DIR,
    CONSTRAINTS_DIR,
)

from saw.utils.formatters import (
    prettify_enum,
)


def on_workbench(workbench):
    p = workbench / '.workbench'
    return p.exists()


def validate_requirement_tag(tag, workbench):
    stop_if_not_workbench(workbench)
    stop_if_invalid_tag(tag)
    stop_if_req_exists(tag, workbench)
    stop_if_parent_req_not_exists(tag, workbench)


def validate_decision_tag(tag, workbench):
    stop_if_not_workbench(workbench)
    stop_if_invalid_tag(tag)
    stop_if_dec_exists(tag, workbench)
    stop_if_parent_dec_not_exists(tag, workbench)


def validate_constraint_tag(tag, workbench):
    stop_if_not_workbench(workbench)
    stop_if_invalid_tag(tag)
    stop_if_con_exists(tag, workbench)
    stop_if_parent_con_not_exists(tag, workbench)


def stop_if_headers_inside_description(obj):
    if '# ' in obj.get('description', ''):
        msg = f'Description of "{obj["title"]}" has Markdown headers, but MUST NOT.'
        raise_error(msg)


def stop_if_invalid_object(obj, schema):
    try:
        return jsonschema.validate(obj, schema)
    except jsonschema.exceptions.ValidationError as error:
        raise_error(str(error))


def stop_if_not_workbench(workbench):
    if not on_workbench(workbench):
        msg = 'You are not inside solution. Change the directory.'
        raise_error(msg)


def stop_if_workbench(workbench):
    if on_workbench(workbench):
        msg = 'Solution already exists.'
        raise_error(msg)


def stop_if_invalid_tag(tag):
    labels = tag.split('.')
    regex = r'^[A-Z][A-Za-z0-9]*$'
    pattern = re.compile(regex)
    for label in labels:
        if not pattern.match(label):
            msg = f'Label {label} in tag {tag} does not match to regex {regex}.'
            raise_error(msg)


def stop_if_parent_req_not_exists(tag, workbench):
    stop_if_no_parent(tag, workbench, REQUIREMENTS_DIR)


def stop_if_parent_dec_not_exists(tag, workbench):
    stop_if_no_parent(tag, workbench, DECISIONS_DIR)


def stop_if_parent_con_not_exists(tag, workbench):
    stop_if_no_parent(tag, workbench, CONSTRAINTS_DIR)


def stop_if_req_exists(tag, workbench):
    path = workbench / REQUIREMENTS_DIR / tag
    if path.exists():
        msg = f'Requirement with tag "{tag}" already exists.'
        raise_error(msg)


def stop_if_dec_exists(tag, workbench):
    path = workbench / DECISIONS_DIR / tag
    if path.exists():
        msg = f'Architecture decision with tag "{tag}" already exists.'
        raise_error(msg)


def stop_if_req_not_exists(tag, workbench):
    path = workbench / REQUIREMENTS_DIR / tag.replace('.', '/')
    if not path.exists():
        msg = f'Requirement with tag "{tag}" does not exist.'
        raise_error(msg)


def stop_if_con_not_exists(tag, workbench):
    path = workbench / CONSTRAINTS_DIR / tag.replace('.', '/')
    if not path.exists():
        msg = f'Constraint with tag "{tag}" does not exist.'
        raise_error(msg)


def stop_if_con_exists(tag, workbench):
    path = workbench / CONSTRAINTS_DIR / tag
    if path.exists():
        msg = f'Constraint with tag "{tag}" already exists.'
        raise_error(msg)


def stop_if_dec_not_exists(tag, workbench):
    path = workbench / DECISIONS_DIR / tag.replace('.', '/')
    if not path.exists():
        msg = f'Architecture decision with tag "{tag}" does not exist.'
        raise_error(msg)


def stop_if_no_parent(tag, workbench, type_):
    parent_path = workbench / type_ / '/'.join(tag.split('.')[:-1])
    if not parent_path.exists():
        msg = f'Parent for "{tag}" does not exist.'
        raise_error(msg)


def stop_if_invalid_stakeholders(global_stakeholders, collection):
    for obj in collection:
        tag = obj['tag']
        stakeholders = obj.get('stakeholders', [])
        for stakeholder in stakeholders:
            if stakeholder not in global_stakeholders:
                msg = f'in "{tag}". "{stakeholder}" is not stakeholder.'
                raise_error(msg)


def warn_if_not_all_requirements_covered(requirements, decision):
    req_tags = set()
    for req in requirements:
        if not req['is_container']:
            req_tags.add(req['tag'])

    for dec in decision:
        tags = []
        if not dec.get('rejected'):
            for req in dec.get('requirements', []):
                tags.append(req['tag'])

        req_tags -= set(tags)

    if req_tags:
        reason = 'This requirements are not covered by architecture decisions'
        msg = f'{reason}: {prettify_enum(req_tags)}'
        display_warning(msg)
        return (reason, req_tags)


def warn_if_stakeholder_without_requirements(solution, requirements):
    sh_without_reqs = []
    for sh in solution['stakeholders']:
        associated = False
        for req in requirements:
            if sh in req['stakeholders']:
                associated = True

        if not associated:
            sh_without_reqs.append(sh)

    if sh_without_reqs:
        reason = 'This shakeholders are not associated with requirements'
        msg = f'{reason}: {prettify_enum(sh_without_reqs)}'
        display_warning(msg)
        return (reason, sh_without_reqs)


def warn_if_dec_does_not_cover_requirement(decisions):
    decs_without_reqs = []
    for dec in decisions:
        if not dec.get('requirements', []) and not dec['is_container']:
            decs_without_reqs.append(dec['tag'])

    if decs_without_reqs:
            reason = 'This decision does not cover any requirements'
            msg = f'{reason}: {prettify_enum(set(decs_without_reqs))}'
            display_warning(msg)
            return (reason, decs_without_reqs)


def warn_if_req_does_not_have_stakeholders(requirements):
    reqs_without_sh = []
    for req in requirements:
        if not req.get('stakeholders', []) and not req['is_container']:
            reqs_without_sh.append(req['tag'])

    if reqs_without_sh:
        reason = 'This requirements does not have stakeholders'
        msg = f'{reason}: {prettify_enum(set(reqs_without_sh))}'
        display_warning(msg)
        return (reason, reqs_without_sh)


def warn_if_solution_has_unapproved_requirements(requirements):
    proposed = []
    for req in requirements:
        if req['status'] == 'draft' and not req['is_container']:
            proposed.append(req['tag'])

    if proposed:
        reason = 'Solution has unapproved requirements'
        msg = f'{reason}: {prettify_enum(set(proposed))}'
        display_warning(msg)
        return (reason, proposed)


def warn_if_solution_has_unapproved_decisions(decisions):
    proposed = []
    for dec in decisions:
        if dec['status'] == 'draft' and not dec['is_container']:
            proposed.append(dec['tag'])

    if proposed:
        reason = 'Solution has unapproved decisions'
        msg = f'{reason}: {prettify_enum(set(proposed))}'
        display_warning(msg)
        return (reason, proposed)


def warn_if_solution_has_unapproved_constraints(constraints):
    proposed = []
    for con in constraints:
        if con['status'] == 'draft' and not con['is_container']:
            proposed.append(con['tag'])

    if proposed:
        reason = 'Solution has unapproved constraints'
        msg = f'{reason}: {prettify_enum(set(proposed))}'
        display_warning(msg)
        return (reason, proposed)


def warn_if_solution_without_requirements(requirements):
    if not requirements:
        msg = 'Solution has no requirements. Please add at least one.'
        display_warning(msg)
        return True


def unexistent_requirement(decision, requirement):
        msg = f'Decision "{decision}" relies on non-existent requirement "{requirement}".'
        raise_error(msg)