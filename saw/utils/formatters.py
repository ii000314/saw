import re


def titlefy_name(text):
    """Makes title from the identifier"""
    pattern = '_|-| '
    parts = re.split(pattern, text)
    capitalized = map(lambda p: p.capitalize(), parts)
    title = ' '.join(capitalized)
    return title


def titlefy_tag(text):
    """Makes title from the tag"""
    pattern = '[A-Z][^A-Z]*'
    parts = re.findall(pattern, text)
    capitalized = map(lambda p: p.capitalize(), parts)
    filtered = filter(lambda p: p != '', capitalized)
    title = ' '.join(filtered)
    return title


def prettify_enum(enum):
    """Makes enumeration printable"""
    printable = '\n'
    for item in enum:
        printable += f'     - {item}\n'
    return printable