import plantuml

from saw.utils.loaders import (
    load_repo,
    load_solution,
    load_requirements,
    load_constraints,
    load_decisions,
    load_template,
    load_diagram_from_cache,
)

from saw.utils.renderers import (
    render_template
)

from saw.utils.validators import (
    warn_if_not_all_requirements_covered,
    warn_if_stakeholder_without_requirements,
    warn_if_req_does_not_have_stakeholders,
    warn_if_solution_has_unapproved_requirements,
    warn_if_solution_has_unapproved_decisions,
    warn_if_solution_has_unapproved_constraints,
    warn_if_solution_without_requirements,
    unexistent_requirement,
)


def build_context(workbench):
    repo = load_repo(workbench)
    solution = load_solution(workbench)
    requirements = load_requirements(workbench)
    decisions = load_decisions(workbench)
    constraints = load_constraints(workbench)

    scope_of_work = []
    for dec in decisions:
        if 'estimation' in dec and not dec.get('rejected'):
            record = (dec['tag'], dec['title'], dec['estimation'])
            scope_of_work.append(record)

    # Add decisions to Requirements
    for req in requirements:
        req['decisions'] = []
        for dec in decisions:
            if req['tag'] in dec.get('requirements', []):
                req['decisions'] += [dec]

        if req.get('rejected'):
            req['status'] = 'rejected'
        elif req.get('stakeholders') and set(req['stakeholders']).issubset(set(req.get('approved', []))):
            req['status'] = 'approved'
        else:
            req['status'] = 'draft'


    # Add stakeholders to Decisions

    for dec in decisions:
        decision_requirements = []
        for req_tag in dec.get('requirements', []):
            for req_full in requirements:
                if req_tag == req_full['tag']:
                    decision_requirements.append(req_full)
                    break
            else:
                unexistent_requirement(dec['title'], req_tag)
        dec['requirements'] = decision_requirements

        decision_stakeholders = []
        for dec_req in dec.get('requirements', []):
            for req in requirements:
                if req['tag'] == dec_req['tag']:
                    decision_stakeholders += req['stakeholders']
        dec['stakeholders'] = list(set(decision_stakeholders)) # dedup

        if dec.get('rejected'):
            dec['status'] = 'rejected'
        elif dec.get('stakeholders') and set(dec['stakeholders']).issubset(set(dec.get('approved', []))):
            dec['status'] = 'approved'
        else:
            dec['status'] = 'draft'

    # Process constraints

    for con in constraints:
        if con.get('rejected'):
            con['status'] = 'rejected'
        elif con.get('stakeholders') and set(con['stakeholders']).issubset(set(con.get('approved', []))):
            con['status'] = 'approved'
        else:
            con['status'] = 'draft'


    # Associate requirements and decisions to stakeholders
    solution['associated_requirements'] = {}
    solution['associated_decisions'] = {}

    for sh in solution['stakeholders']:
        reqs = []
        for req in requirements:
            if (sh in req['stakeholders'] and sh not in req.get('approved', [])) and (req['status'] != 'rejected'):
                reqs.append(req)
        solution['associated_requirements'][sh] = reqs

        decs = []
        for dec in decisions:
            if (sh in dec['stakeholders'] and sh not in dec.get('approved', [])) and (dec['status'] != 'rejected'):
                decs.append(dec)
        solution['associated_decisions'][sh] = decs

    stakeholder_without_requirements = warn_if_stakeholder_without_requirements(solution, requirements)
    req_does_not_have_stakeholders = warn_if_req_does_not_have_stakeholders(requirements)
    not_all_requirements_covered = warn_if_not_all_requirements_covered(requirements, decisions)
    solution_has_unapproved_requirements = warn_if_solution_has_unapproved_requirements(requirements)
    solution_has_unapproved_decisions = warn_if_solution_has_unapproved_decisions(decisions)
    solution_without_requirements = warn_if_solution_without_requirements(requirements)
    solution_has_unapproved_constraints = warn_if_solution_has_unapproved_constraints(constraints)

    # Check if all archirecture dicisios are approved
    dec_are_approved = [dec['status'] != 'draft' or dec['is_container'] for dec in decisions]

    if all(dec_are_approved) and \
        not solution_without_requirements and \
        not stakeholder_without_requirements and \
        not req_does_not_have_stakeholders and \
        not not_all_requirements_covered and \
        not solution_has_unapproved_requirements and \
        not solution_has_unapproved_decisions and \
        not solution_has_unapproved_constraints:
        solution['status'] = 'APPROVED'
    else:
        solution['status'] = 'IN PROGRESS'

    potencial_warnings = [
        stakeholder_without_requirements,
        req_does_not_have_stakeholders,
        not_all_requirements_covered,
        solution_has_unapproved_requirements,
        solution_has_unapproved_decisions,
        solution_has_unapproved_constraints,
    ]
    warnings = [warning for warning in potencial_warnings if warning]
    solution['warnings'] = warnings

    context = {
        'repo': repo,
        'solution': solution,
        'requirements': requirements,
        'decisions': decisions,
        'constraints': constraints,
        'scope_of_work': scope_of_work,
    }

    return context


def build_diagrams(server, context):
    puml = plantuml.PlantUML(server)

    chapters = [
        'decisions'
    ]
    diagrams = {}

    for chapter in chapters:
        diagrams[chapter] = []
        for obj in context[chapter]:
            tag = obj.get('tag')
            diagram = obj.get('diagram')

            if diagram:
                workbench = context['repo']['location']
                if cached := load_diagram_from_cache(tag, workbench):
                    image = cached
                else:
                    image = puml.processes(diagram)

                diagrams[chapter].append({
                    'name': tag,
                    'image': image,
                })

    return diagrams


def build_html(context):
    template = load_template('document.html')
    document = render_template(template, context)
    return document


def build_confluence(context):
    template = load_template('document.confluence')
    document = render_template(template, context)
    return document