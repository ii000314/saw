"""Set of utility functions which implements trees behavior"""

def flatten(tree):
    collection = []
    for item in tree:
        children = item.pop('children')
        requirements = item.get('requirements', [])
        stakehoders = item.get('stakeholders', [])

        if children and \
                not requirements and \
                not stakehoders:
            item['is_container'] = True
        else:
            item['is_container'] = False
        collection.append(item)
        collection += flatten(children)

    return collection
