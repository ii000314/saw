# Solution Architect Workbench

## Development
Install locally in developer mode
```bash
pip install --editable .
```
Run tests
```bash
make test
```

## Usage

### Create new solution
```bash
saw new <solution>
```

### Add requirement
```bash
saw req <tag>
```
where **tag** is hierarchical identifier, for example: "Level1", "Level1.Level2" and so on

### Add architecture decision
```bash
saw dec <tag>
```
where **tag** is hierarchical identifier, for example: "Level1", "Level1.Level2" and so on

### Build document
```bash
saw build
```

### Upload document to archive
```bash
saw upload
```

### Move object
```bash
saw move req Test.Test Hello.Test
```

```bash
saw move dec Test.Test Hello.Test
```

### Make presentation template
```bash
saw presentation
```